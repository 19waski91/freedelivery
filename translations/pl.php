<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_0b429f0237958e43e5bf20c22f3abf23'] = 'Darmowa dostawa';
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_a4bf477e0136caf22f9d861c458c0ef5'] = 'Wartość darmowej dostawy jest niepoprawna';
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_ccab97830a0c6d01604c1df0b45988cb'] = 'Dostep hook';
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_ac19da4ba9cc1fc60e74aab5cee936ea'] = 'Wybierz hooki do wyświetlania';
$_MODULE['<{freedelivery}default-bootstrap>freedelivery_7e92be57f7e5966cd224770e8f4d5342'] = 'Aby dodać własny hook, musisz dodać kod we właściwym miejscu w danym pliku szablonu.';
$_MODULE['<{freedelivery}default-bootstrap>free-shipping_e46b3b8448b2d64480e779a7a0ea9586'] = 'Wartość koszyka:';
$_MODULE['<{freedelivery}default-bootstrap>free-shipping_13ca5a582444778adb0e562204dd80e1'] = ', do darmowej dostawy brakuje:';
