<?php

if (!defined('_PS_VERSION_'))
    exit;

class FreeDelivery extends Module
{
    public function __construct()
    {
        $this->name = 'freedelivery';
        $this->tab = 'other';
        $this->version = '1.1.0';
        $this->author = 'Łukasz Machowski';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Free delivery');
        $this->description = $this->l('Free delivery');
        $this->context->smarty->assign('module_name', $this->name);
        $this->confirmUninstall = $this->l('Czy na pewno chcesz odinstalować?');
        $this->ps_versions_compliancy = array('min' => '1.6.1.5', 'max' => _PS_VERSION_);

        if (!Configuration::get('freedelivery'))
            $this->warning = $this->l('No name provided');
    }

    public function runContent($idHook){

        $hooksActive = Configuration::get('PS_SHIPPING_FREE_DISPLAY_HOOKS', []);
        if (isset($hooksActive) && $hooksActive){
            $hooksActive = explode('|', $hooksActive);
        }

        if (isset($hooksActive) && is_array($hooksActive) && in_array($idHook, $hooksActive)){
            return $this->getContentFreeShipping();
        }

        return false;
    }

    public function getContentFreeShipping(){

        $cart = Context::getContext()->cart;
        if (!is_null($cart)){
            $data = [
                'productValue' => $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS)
                    - $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS),
                'freeShippingPrice' => Configuration::get('PS_SHIPPING_FREE_PRICE'),
                'toFreeShippingPrice' => $this->_getTotalFreeShipping(),
            ];

            $this->smarty->assign($data);
        }

        return $this->display(__FILE__, 'free-shipping.tpl');
    }

    private function _getTotalFreeShipping()
    {
        $cart = Context::getContext()->cart;
        $total_free_shipping = 0;
        if ($free_shipping = Tools::convertPrice(floatval(Configuration::get('PS_SHIPPING_FREE_PRICE')), Context::getContext()->currency))
        {
            $total_free_shipping =  floatval($free_shipping - ($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS) -
                    $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS)));

            if ($total_free_shipping < 0)
                $total_free_shipping = 0;
        }

        return $total_free_shipping;
    }

    public function getContent()
    {
        if (Tools::isSubmit('saveFreeDelivery')){

            $freeDelivery = Tools::getValue('freeDelivery', 0);
            if (!ValidateCore::isUnsignedFloat($freeDelivery)){
                $this->displayError($this->l('Free delivery has incorrect value'));
            }else{
                Configuration::updateGlobalValue('PS_SHIPPING_FREE_PRICE', $freeDelivery);
            }

            if (Tools::isSubmit('groupBox')){
                Configuration::updateGlobalValue('PS_SHIPPING_FREE_DISPLAY_HOOKS', implode('|', Tools::getValue('groupBox', array())));
            }else{
                Configuration::updateGlobalValue('PS_SHIPPING_FREE_DISPLAY_HOOKS', '');
            }

        }
        return $this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'tinymce' => true,
                    'title' => $this->l('Free delivery'),
                    'icon' => 'icon-cogs'
                ),
                'input' => $this->generateInputFenderForm(),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),

            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = 'saveFreeDelivery';
        $helper->identifier = $this->identifier;
        $helper->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );


        $helper->tpl_vars['fields_value']['freeDelivery'] = Configuration::get('PS_SHIPPING_FREE_PRICE');
        $helper->tpl_vars['fields_value']['label'] = $this->l('{hook h="freeDelivery"}');

        $hooksActive = Configuration::get('PS_SHIPPING_FREE_DISPLAY_HOOKS', []);
        if (isset($hooksActive) && $hooksActive){
            $hooksActive = explode('|', $hooksActive);
        }else{
            $hooksActive = [];
        }

        foreach ($this->getHooks() as $hook){
            $helper->tpl_vars['fields_value']['groupBox_'.$hook['id_group']] = in_array($hook['id_group'], $hooksActive) ? true : false;
        }


        return $helper->generateForm(array($fields_form));
    }

    public function hookFreeDelivery(){

    }

    public function hookHeader()
    {
        Media::addJsDef(array(
                'free_delivery_link' => $this->context->link->getModuleLink(
                    'freedelivery',
                    'freedelivery',
                    [
                        'ajax' => true,
                        'action' => 'getFreeDelivery',
                    ])));

        if (_PS_VERSION_ >= '1.7.0'){
            $this->context->controller->addJS(_PS_MODULE_DIR_.'freedelivery/views/js/freedelivery17.js');
        }else{
            $this->context->controller->addJS(_PS_MODULE_DIR_.'freedelivery/views/js/freedelivery16.js');
        }
    }

    private function generateInputFenderForm(){

        $inputs = array();
        $hooks = $this->getHooks();

        $inputs[] = array(
            'type' => 'group',
            'label' => $this->l('Hook access'),
            'title' => $this->l('Hook access'),
            'name' => 'hooks',
            'values' => $hooks,
            'required' => true,
            'col' => '6',
            'hint' => $this->l('Select hooks to display.')
        );

        $inputs[] = array(
            'type' => 'text',
            'label' => $this->l('Free delivery'),
            'name' => 'freeDelivery',
            'lang' => false,
        );

        $inputs[] = array(
            'type' => 'free',
            'name' => 'label',
            'label' => $this->l('To add your own hook, you must add the code to the right place in the given TPL file'),
        );

        return $inputs;
    }

    public function getHooks(){

        $availableHooks = ['displayTop',
                            'displayTopColumn',
                            'freeDelivery',
                            'displayNav',
                            'displayLeftColumn',
                            'displayRightColumn',
                            'displayFooter',
                            'displayHome',
                            'displayShoppingCart',
                            'displayCustomerAccountForm',
            ];

        $hooks = HookCore::getHooks();

        if (isset($hooks) && !empty($hooks)){
            foreach ($hooks as $key => &$hook){
                if (in_array($hook['name'], $availableHooks)) {

                    $hook['id_group'] = $hook['id_hook'];
                    unset($hook['id_hook']);
                    unset($hook['title']);
                    unset($hook['description']);
                    unset($hook['position']);
                    unset($hook['live_edit']);
                }else{
                    unset($hooks[$key]);
                }
            }
        }

        return $hooks;
    }

    public function install()
    {
        return parent::install()
            && $this->registerHooks()
            && $this->installSqls();
    }

    public function registerHooks()
    {
        $return = true;
        $return &= $this->registerHook('freeDelivery');
        $return &= $this->registerHook('header');
        return $return;
    }

    public function installSqls()
    {
        $sqls = array();
        $result = true;
        $db = Db::getInstance();

        foreach ($sqls as $sql)
            $result &= $db->execute($sql);

        return $result;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallDb())
            return false;

        return true;
    }

    private function uninstallDb()
    {
        $sqls = array();
        $db = Db::getInstance();

        $result = true;
        foreach ($sqls as $sql)
            $result &= $db->execute($sql);

        return $result;
    }
}