<div class="free-delivery-hook-content">
    {if isset($toFreeShippingPrice) && $toFreeShippingPrice > 0 && isset($freeShippingPrice)}
        <div style="width:300px">
            {if isset($productValue)}
                <span>
                    {l s='Product value:' mod='freedelivery'} {displayPrice price=$productValue|string_format:"%.2f"}
                </span>
            {/if}
            <span>
                {l s='To free shipping missing:' mod='freedelivery'}
            </span>
            <span>
                    {displayPrice price=$toFreeShippingPrice|string_format:"%.2f"}
            </span>
            {assign var=productsValue value=$freeShippingPrice-$toFreeShippingPrice|string_format:"%.2f"}
            {assign var=freeShippingLength value=$productsValue/$freeShippingPrice*100}
            {assign var=tofreeShipping value=$freeShippingPrice-$productsValue}

            <div>
                <div class="progress" style="height: 10px">
                    <div class="progress-bar" role="progressbar" aria-valuenow=""
                         aria-valuemin="0" aria-valuemax="100" style="width:{$freeShippingLength}%; background-color: #76A548">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>