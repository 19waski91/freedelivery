$(document).ready(function(){

    $('.cart_block_total').on('DOMSubtreeModified',function(){

        $.ajax({
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            async: true,
            cache: false,
            dataType: "json",
            url: free_delivery_link,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            },
            success: function(jsonData){
                $('.free-delivery-hook-content').html(jsonData.html);
            }
        });
    })
});